import dotenv from "dotenv";
dotenv.config();
import "reflect-metadata";
import { connectToMongo } from "./utils/mongo";
import { createServer } from "./utils/server";

async function bootstrap() {
  const { app } = await createServer();
  console.log("App is listening on http://localhost:4000");
  await connectToMongo();
}

bootstrap();
