import dotenv from "dotenv";
dotenv.config();
import express from "express";
import { Container } from 'typedi';
import { buildSchema } from "type-graphql";
import cookieParser from "cookie-parser";
import { ApolloServer } from "apollo-server-express";
import {
  ApolloServerPluginLandingPageGraphQLPlayground,
  ApolloServerPluginLandingPageProductionDefault,
} from "apollo-server-core";
import { resolvers } from "../resolvers";
import { verifyJwt } from "../utils/jwt";
import { User } from "../schema/user.schema";
import Context from "../types/context";
import authChecker from "../utils/authChecker";


export async function createServer() {
  // Build the schema
  const schema = await buildSchema({
    resolvers,
    authChecker,
    container: Container
  });

  // Init express
  const app = express();
  app.use(cookieParser());

  // Create the apollo server
  const server = new ApolloServer({
    schema,
    context: (ctx: Context) => {
      const context = ctx;

      if (ctx.req.headers.authorization) {
        const user = verifyJwt<User>(ctx.req.headers.authorization);
        context.user = user;
      }
      return context;
    },
    plugins: [
      process.env.NODE_ENV === "production"
        ? ApolloServerPluginLandingPageProductionDefault()
        : ApolloServerPluginLandingPageGraphQLPlayground(),
    ],
  });

  await server.start();
  server.applyMiddleware({ app });
  // app.listen on express server
  await app.listen({ port: 4000 });
  // console.log("App is listening on http://localhost:4000");
  return { app } 
}