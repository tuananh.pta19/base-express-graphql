import supertest from 'supertest';
import { createServer } from "../utils/server";
import { connectToMongo } from "../utils/mongo";
let request: supertest.SuperTest<supertest.Test>;
const GET_PRODUCT = {
  query: `{
    products {
      _id
      name
      price
    }
  }`,
  variables: {}
}

beforeAll(async () => {
  const { app } = await createServer();
  await connectToMongo();
  request = supertest(app);

})
test("Query all products", async () => {
  const res = await request.post('/graphql')
    .send(GET_PRODUCT)
    .set("Accept", "application/json");

  expect(res.status).toBe(200);
  expect(res.body.data.products).not.toBeUndefined();
  expect(res.body.data.products.length).toBeGreaterThanOrEqual(0);
});
